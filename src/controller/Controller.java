package controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import model.*;
import storage.Storage;

public class Controller {
	public static ArrayList<ProductCategory> getProductCategories() {
		return Storage.getProductCategories();
	}

	public static Controller getTestController() {
		return new Controller();
	}

	public static void deleteProductCategory(ProductCategory productCategory) {
		if (productCategory == null) {
			throw new NullPointerException("Kategori er Null");
		}
		Storage.removeProductCategory(productCategory);
	}

	public static ProductCategory createProductCategory(String category) {
		if (category == null) {
			throw new NullPointerException("Kategori er Null");
		}
		if (!category.isEmpty()) {
			boolean productCategoryExists = false;
			for (ProductCategory pC : Storage.getProductCategories()) {
				if (category.equalsIgnoreCase(pC.getCategory())) {
					productCategoryExists = true;
					break;
				}
			}
			if (!productCategoryExists) {
				ProductCategory productCategory = new ProductCategory(category);
				Storage.addProductCategory(productCategory);
				return productCategory;
			}
			else if (productCategoryExists) {
				throw new IllegalArgumentException("Ingen dubletter tilladt");
			}
		}
		throw new IllegalArgumentException("Feltet må ikke være tomt");
	}

	public static void updateProductCategory(ProductCategory productCategory, String category) {
		if (productCategory == null || category == null) {
			throw new NullPointerException("Kategori er Null");
		}
		if (!category.isEmpty()) {
			boolean productCategoryExists = false;
			for (ProductCategory pC : Storage.getProductCategories()) {
				if (category.equalsIgnoreCase(pC.getCategory())) {
					productCategoryExists = true;
					break;
				}
			}
			if (!productCategoryExists) {
				productCategory.setCategory(category);
			}
			else if (productCategoryExists) {
				throw new IllegalArgumentException("Kategorien: " + category + ", eksisterer allerede");
			}
		}
		else {
			throw new IllegalArgumentException("Feltet må ikke være tomt");
		}
	}

	public static ArrayList<PriceList> getPriceLists() {
		return Storage.getPriceLists();
	}

	public static void deletePriceList(PriceList priceList) {
		if (priceList == null) {
			throw new NullPointerException("Prisliste er Null");
		}
		Storage.removePriceList(priceList);
	}

	public static PriceList createPriceList(String name) {
		if (name == null) {
			throw new NullPointerException("Prisliste er Null");
		}
		if (!name.isEmpty()) {
			boolean priceListExists = false;
			for (PriceList pL : Storage.getPriceLists()) {
				if (name.equalsIgnoreCase(pL.getName())) {
					priceListExists = true;
					break;
				}
			}
			if (!priceListExists) {
				PriceList priceList = new PriceList(name);
				Storage.addPriceList(priceList);
				return priceList;
			}
			else if (priceListExists) {
				throw new IllegalArgumentException("Ingen dubletter tilladt");
			}
		}
		throw new IllegalArgumentException("Feltet må ikke være tomt");
	}

	public static void updatePriceList(PriceList priceList, String name) {
		if (priceList == null || name == null) {
			throw new NullPointerException("Navn er Null");
		}
		if (!name.isEmpty()) {
			boolean priceListExists = false;
			for (PriceList pL : Storage.getPriceLists()) {
				if (name.equalsIgnoreCase(pL.getName())) {
					priceListExists = true;
					break;
				}
			}
			if (!priceListExists) {
				priceList.setName(name);
			}
			else if (priceListExists) {
				throw new IllegalArgumentException("Prislisten: " + name + ", eksisterer allerede");
			}
		}
		else {
			throw new IllegalArgumentException("Feltet må ikke være tomt");
		}
	}

	public static Product createProduct(String name, ProductCategory productCategory) {
		if (name == null || productCategory == null) {
			throw new NullPointerException("Navn eller Kategori er Null");
		}
		if (!name.isEmpty()) {
			boolean productExists = false;
			for (Product p : Storage.getProducts()) {
				if (name.equalsIgnoreCase(p.getName()) && productCategory.equals(p.getProductCategory())) {
					productExists = true;
					break;
				}
			}
			if (!productExists) {
				Product product = new Product(name, productCategory);
				Storage.addProduct(product);
				return product;
			}
			else if (productExists) {
				throw new IllegalArgumentException("Ingen dubletter tilladt");
			}
		}
		throw new IllegalArgumentException("Feltet må ikke være tomt");
	}

	public static void updateProduct(Product product, String name) {
		if (product == null || name == null) {
			throw new NullPointerException("Produkt er Null");
		}
		if (!name.isEmpty()) {
			boolean productExists = false;
			for (Product p : Storage.getProducts()) {
				if (name.equalsIgnoreCase(p.getName())) {
					productExists = true;
					break;
				}
			}
			if (!productExists) {
				product.setName(name);
			}
			else if (productExists) {
				throw new IllegalArgumentException("Produktet: " + name + ", eksisterer allerede");
			}
		}
		else {
			throw new IllegalArgumentException("Feltet må ikke være tomt");
		}
	}

	public static ArrayList<Product> getProducts() {
		return Storage.getProducts();
	}

	public static void deleteProduct(Product product) {
		if (product == null) {
			throw new NullPointerException("Produkt er Null");
		}
		Storage.getProducts().remove(product);
	}

	public static Price createPrice(double price, PriceList priceList, Product product) {
		if (priceList == null || product == null) {
			throw new NullPointerException("Prisliste eller Produkt er Null");
		}
		if (price >= 0) {
			Price newPrice = new Price(price, product);
			priceList.addPrice(newPrice);
			return newPrice;
		}
		throw new IllegalArgumentException("Prisen må ikke være negativ");
	}

	public static void removePrice(PriceList priceList, Product product) {
		if (priceList == null || product == null) {
			throw new NullPointerException("Prisliste eller Produkt er Null");
		}
		List<Price> prices = priceList.getPriceListPrices();
		for (Iterator<Price> itr = prices.iterator(); itr.hasNext();) {
			Price priceitem = itr.next();
			if (priceitem.getProduct() == product) {
				itr.remove();
			}
		}
	}

	public static ArrayList<Sale> getSales() {
		return Storage.getSales();
	}

	public static Sale createSale(String paymentMethod) {
		if (paymentMethod == null) {
			throw new NullPointerException("Betalingsmetode er Null");
		}
		if (!paymentMethod.isEmpty()) {
			Sale sale = new Sale(paymentMethod);
			Storage.addSales(sale);
			return sale;
		}
		throw new IllegalArgumentException("Feltet må ikke være tomt");
	}

	public static Checkout createCheckout(double price, int amount, Product product) {
		if (product == null) {
			throw new NullPointerException("Produkt er Null");
		}
		if (price >= 0 && amount > 0) {
			Checkout checkout = new Checkout(price, amount, product);
			Storage.addCheckout(checkout);
			return checkout;
		}
		throw new IllegalArgumentException("Antal må ikke være 0 eller negativ og Pris må ikke være negativ");
	}

	public static ArrayList<Checkout> getCheckouts() {
		return Storage.getCheckouts();
	}

	public static void deleteCheckout(Checkout checkout) {
		if (checkout == null) {
			throw new NullPointerException("Checkout er Null");
		}
		Storage.removeCheckout(checkout);
	}

	public static double calculatePriceForCheckouts(List<Checkout> checkouts) {
		double total = 0.0;
		for (Checkout checkout : checkouts) {
			total += checkout.getProductPriceSum();
		}
		return total;
	}

	public static ArrayList<PriceList> getPriceListWithProduct(Product product) {
		ArrayList<PriceList> collect = new ArrayList<>();
		for (PriceList list : Storage.getPriceLists()) {
			for (Price priceListPrice : list.getPriceListPrices()) {
				if (priceListPrice.getProduct() == product) {
					collect.add(list);
				}
			}
		}
		return collect;
	}

	public static Checkout createCheckoutPercentageDiscount(double price, double discounts, int amount, Product product) {
		double discount = ((price / 100) * (discounts)) * amount;
		Checkout checkout = createCheckout(price - (discount / amount), amount, product);
		checkout.setDiscount(discount);
		return checkout;
	}

	public static Checkout createCheckoutCashDiscount(double price, double discounts, int amount, Product product) {
		Checkout checkout = createCheckout(price - (discounts / amount), amount, product);
		checkout.setDiscount(discounts);
		return checkout;
	}

	public static boolean cashDiscountWithinBoundaries(Double discounts, double price, int amount) {
		return discounts > 0 && discounts <= (price * amount);
	}

	public static boolean percentDiscountWithinBoundaries(Double discounts) {
		return discounts > 0 && discounts <= 100;
	}

	public static void initStorage() {
		PriceList priceList1 = Controller.createPriceList("Butik");
		PriceList priceList2 = Controller.createPriceList("Fredagsbar");
		ProductCategory productCategory1 = Controller.createProductCategory("Flaskeøl");
		ProductCategory productCategory2 = Controller.createProductCategory("Fadøl");
		ProductCategory productCategory3 = Controller.createProductCategory("Spiritus");
		ProductCategory productCategory4 = Controller.createProductCategory("Sodavand");
		ProductCategory productCategory5 = Controller.createProductCategory("Snacks");
		Product category1Product1 = Controller.createProduct("Klosterbryg", productCategory1);
		Product category1Product2 = Controller.createProduct("Sweet Georgia Brown", productCategory1);
		Product category1Product3 = Controller.createProduct("Extra Pilsner", productCategory1);
		Product category1Product4 = Controller.createProduct("Celebration", productCategory1);
		Product category1Product5 = Controller.createProduct("Blondie", productCategory1);
		Product category1Product6 = Controller.createProduct("Forårsbryg", productCategory1);
		Product category1Product7 = Controller.createProduct("India Pale Ale", productCategory1);
		Product category1Product8 = Controller.createProduct("Julebryg", productCategory1);
		Product category1Product9 = Controller.createProduct("Juletønden", productCategory1);
		Product category1Product10 = Controller.createProduct("Old Strong Ale", productCategory1);
		Product category1Product11 = Controller.createProduct("Fregatten Jylland", productCategory1);
		Product category1Product12 = Controller.createProduct("Imperial Stout", productCategory1);
		Product category1Product13 = Controller.createProduct("Tribute", productCategory1);
		Product category1Product14 = Controller.createProduct("Black Monster", productCategory1);
		Product category2Product1 = Controller.createProduct("Klosterbryg", productCategory2);
		Product category2Product2 = Controller.createProduct("Jazz Classic", productCategory2);
		Product category2Product3 = Controller.createProduct("Extra Pilsner", productCategory2);
		Product category2Product4 = Controller.createProduct("Celebration", productCategory2);
		Product category2Product5 = Controller.createProduct("Blondie", productCategory2);
		Product category2Product6 = Controller.createProduct("Forårsbryg", productCategory2);
		Product category2Product7 = Controller.createProduct("India Pale Ale", productCategory2);
		Product category2Product8 = Controller.createProduct("Julebryg", productCategory2);
		Product category2Product9 = Controller.createProduct("Imperial Stout", productCategory2);
		Product category2Product10 = Controller.createProduct("Special", productCategory2);
		Product category3Product1 = Controller.createProduct("Spirit of Aarhus", productCategory3);
		Product category3Product2 = Controller.createProduct("SOA med pind", productCategory3);
		Product category3Product3 = Controller.createProduct("Whisky", productCategory3);
		Product category3Product4 = Controller.createProduct("Liquor of Aarhus", productCategory3);
		Product category4Product1 = Controller.createProduct("Æblebrus", productCategory4);
		Product category4Product2 = Controller.createProduct("Cola", productCategory4);
		Product category4Product3 = Controller.createProduct("Nikoline", productCategory4);
		Product category4Product4 = Controller.createProduct("7-Up", productCategory4);
		Product category5Product1 = Controller.createProduct("Chips", productCategory5);
		Product category5Product2 = Controller.createProduct("Peanuts", productCategory5);
		createPrice(36, priceList1, category1Product1);
		createPrice(36, priceList1, category1Product2);
		createPrice(36, priceList1, category1Product3);
		createPrice(36, priceList1, category1Product4);
		createPrice(36, priceList1, category1Product5);
		createPrice(36, priceList1, category1Product6);
		createPrice(36, priceList1, category1Product7);
		createPrice(36, priceList1, category1Product8);
		createPrice(36, priceList1, category1Product9);
		createPrice(36, priceList1, category1Product10);
		createPrice(36, priceList1, category1Product11);
		createPrice(36, priceList1, category1Product12);
		createPrice(36, priceList1, category1Product13);
		createPrice(50, priceList1, category1Product14);
		createPrice(300, priceList1, category3Product1);
		createPrice(350, priceList1, category3Product2);
		createPrice(500, priceList1, category3Product3);
		createPrice(175, priceList1, category3Product4);
		createPrice(50, priceList2, category1Product1);
		createPrice(50, priceList2, category1Product2);
		createPrice(50, priceList2, category1Product3);
		createPrice(50, priceList2, category1Product4);
		createPrice(50, priceList2, category1Product5);
		createPrice(50, priceList2, category1Product6);
		createPrice(50, priceList2, category1Product7);
		createPrice(50, priceList2, category1Product8);
		createPrice(50, priceList2, category1Product9);
		createPrice(50, priceList2, category1Product10);
		createPrice(50, priceList2, category1Product11);
		createPrice(50, priceList2, category1Product12);
		createPrice(50, priceList2, category1Product13);
		createPrice(50, priceList2, category1Product14);
		createPrice(30, priceList2, category2Product1);
		createPrice(30, priceList2, category2Product2);
		createPrice(30, priceList2, category2Product3);
		createPrice(30, priceList2, category2Product4);
		createPrice(30, priceList2, category2Product5);
		createPrice(30, priceList2, category2Product6);
		createPrice(30, priceList2, category2Product7);
		createPrice(30, priceList2, category2Product8);
		createPrice(30, priceList2, category2Product9);
		createPrice(30, priceList2, category2Product10);
		createPrice(300, priceList2, category3Product1);
		createPrice(350, priceList2, category3Product2);
		createPrice(500, priceList2, category3Product3);
		createPrice(175, priceList2, category3Product4);
		createPrice(15, priceList2, category4Product1);
		createPrice(15, priceList2, category4Product2);
		createPrice(15, priceList2, category4Product3);
		createPrice(15, priceList2, category4Product4);
		createPrice(10, priceList2, category5Product1);
		createPrice(10, priceList2, category5Product2);
	}
}