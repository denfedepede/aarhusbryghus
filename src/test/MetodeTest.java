package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import controller.Controller;
import model.Checkout;
import model.Product;
import model.ProductCategory;
import model.Sale;
import storage.Storage;

public class MetodeTest {
	Sale sale1;
	Sale sale2;
	Product product1;
	Product product2;
	Checkout checkout1;
	Checkout checkout2;
	ProductCategory productCategory1;
	ArrayList<Checkout> arr = new ArrayList<>();

	// ---------------------------------------------------------------------------------------------------------------------------------
	@Before
	public void setUp() {
		productCategory1 = Controller.createProductCategory("TestKategori");
		product1 = Controller.createProduct("Klosterbryg", productCategory1);
		product2 = Controller.createProduct("Blondie", productCategory1);
		checkout1 = Controller.createCheckout(30, 2, product1);
		checkout2 = Controller.createCheckout(30, 4, product2);
		sale1 = Controller.createSale("Kontant");
		sale1.addCheckout(Storage.getCheckouts());
	}

	@After
	public void cleanUp() {
		Controller.deleteCheckout(checkout1);
		Controller.deleteCheckout(checkout2);
		Controller.deleteProduct(product1);
		Controller.deleteProduct(product2);
		Controller.deleteProductCategory(productCategory1);
		Storage.removeSales(sale1);
	}

	// ---------------------------------------------------------------------------------------------------------------------------------
	@Test
	public void TC1TotalPrice() {
		assertEquals(sale1.totalPrice(), 180, 0.001);
	}

	@Test
	public void TC2AddCheckout() {
		assertEquals(sale1.getCheckouts(), Controller.getCheckouts());
	}

	@Test
	public void TC3GetProductPriceSum() {
		assertEquals(checkout1.getProductPriceSum(), 60, 0.001);
	}
}