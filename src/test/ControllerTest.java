package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import controller.Controller;
import model.Checkout;
import model.Price;
import model.PriceList;
import model.Product;
import model.ProductCategory;
import model.Sale;
import storage.Storage;

public class ControllerTest {
	ProductCategory productCategory1;
	ProductCategory productCategory2;
	PriceList priceList1;
	PriceList priceList2;
	Product product1;
	Product product2;
	Price price1;
	Checkout checkout1;
	Checkout checkout2;
	Sale sale1;

	// ---------------------------------------------------------------------------------------------------------------------------------
	@Before
	public void setUp() {
		this.productCategory1 = Controller.createProductCategory("TestCategory");
		this.priceList1 = Controller.createPriceList("TestPriceList");
		this.product1 = Controller.createProduct("TestProduct", productCategory1);
		this.price1 = Controller.createPrice(30, priceList1, product1);
		this.checkout1 = Controller.createCheckout(price1.getPrice(), 5, product1);
		this.checkout2 = Controller.createCheckout(price1.getPrice(), 3, product1);
		this.sale1 = Controller.createSale("Kontant");
	}

	@After
	public void cleanUp() {
		Storage.removeProductCategory(productCategory1);
		Storage.removeProductCategory(productCategory2);
		Storage.removePriceList(priceList1);
		Storage.removePriceList(priceList2);
		Storage.removeProduct(product1);
		Storage.removeProduct(product2);
		Storage.removeCheckout(checkout1);
		Storage.removeCheckout(checkout2);
		Storage.removeSales(sale1);
	}

	// ---------------------------------------------------------------------------------------------------------------------------------
	@Test
	public void TC1CreateProductCategory() {
		assertEquals(Storage.getProductCategories().get(0).getCategory(), "TestCategory");
	}

	@Test
	public void TC2CreateProductCategoryNull() {
		try {
			Controller.createProductCategory(null);
		}
		catch (NullPointerException e) {
			assertEquals("Kategori er Null", e.getMessage());
		}
	}

	@Test
	public void TC3CreateProductCategoryEmpty() {
		try {
			productCategory1 = Controller.createProductCategory("");
		}
		catch (IllegalArgumentException e) {
			assertEquals("Feltet må ikke være tomt", e.getMessage());
		}
	}

	@Test
	public void TC4CreateProductCategoryDuplicates() {
		try {
			productCategory2 = Controller.createProductCategory("TestCategory");
		}
		catch (IllegalArgumentException e) {
			assertEquals("Ingen dubletter tilladt", e.getMessage());
		}
	}

	@Test
	public void TC5UpdateProductCategory() {
		Controller.updateProductCategory(productCategory1, "TestCategory1");
		assertEquals(Storage.getProductCategories().get(0).getCategory(), "TestCategory1");
	}

	@Test
	public void TC6UpdateProductCategoryNullAndCategoryNull() {
		try {
			Controller.updateProductCategory(null, null);
		}
		catch (NullPointerException e) {
			assertEquals("Kategori er Null", e.getMessage());
		}
	}

	@Test
	public void TC7UpdateProductCategoryNullAndCategoryValue() {
		try {
			Controller.updateProductCategory(null, "TestCategory");
		}
		catch (NullPointerException e) {
			assertEquals("Kategori er Null", e.getMessage());
		}
	}

	@Test
	public void TC8UpdateProductCategoryValueAndCategoryNull() {
		try {
			Controller.updateProductCategory(productCategory1, null);
		}
		catch (NullPointerException e) {
			assertEquals("Kategori er Null", e.getMessage());
		}
	}

	@Test
	public void TC9UpdateProductCategoryEmpty() {
		try {
			Controller.updateProductCategory(productCategory1, "");
		}
		catch (IllegalArgumentException e) {
			assertEquals("Feltet må ikke være tomt", e.getMessage());
		}
	}

	@Test
	public void TC10UpdateProductCategoryDuplicates() {
		productCategory2 = Controller.createProductCategory("TestCategory1");
		try {
			Controller.updateProductCategory(productCategory2, "TestCategory");
		}
		catch (IllegalArgumentException e) {
			assertEquals("Kategorien: " + productCategory1 + ", eksisterer allerede", e.getMessage());
		}
	}

	@Test
	public void TC11DeleteProductCategory() {
		assertEquals(Storage.getProductCategories().size(), 1);
		assertEquals(Storage.getProductCategories().get(0).getCategory(), "TestCategory");
		Controller.deleteProductCategory(productCategory1);
		assertEquals(Storage.getProductCategories().size(), 0);
	}

	@Test
	public void TC12DeleteProductCategoryNull() {
		try {
			Controller.deleteProductCategory(null);
		}
		catch (NullPointerException e) {
			assertEquals("Kategori er Null", e.getMessage());
		}
	}

	// ---------------------------------------------------------------------------------------------------------------------------------
	@Test
	public void TC13CreatePriceList() {
		assertEquals(priceList1.getName(), "TestPriceList");
	}

	@Test
	public void TC14CreatePriceListNull() {
		try {
			Controller.createPriceList(null);
		}
		catch (NullPointerException e) {
			assertEquals("Prisliste er Null", e.getMessage());
		}
	}

	@Test
	public void TC15CreatePriceListEmpty() {
		try {
			Controller.createPriceList("");
		}
		catch (IllegalArgumentException e) {
			assertEquals("Feltet må ikke være tomt", e.getMessage());
		}
	}

	@Test
	public void TC16CreatePriceListDuplicates() {
		try {
			priceList2 = Controller.createPriceList("TestPriceList");
		}
		catch (IllegalArgumentException e) {
			assertEquals("Ingen dubletter tilladt", e.getMessage());
		}
	}

	@Test
	public void TC17UpdatePriceList() {
		Controller.updatePriceList(priceList1, "TestPriceList1");
		assertEquals(Storage.getPriceLists().get(0).getName(), "TestPriceList1");
	}

	@Test
	public void TC18UpdatePriceListNullAndNameNull() {
		try {
			Controller.updatePriceList(null, null);
		}
		catch (NullPointerException e) {
			assertEquals("Navn er Null", e.getMessage());
		}
	}

	@Test
	public void TC19UpdatePriceListNullAndNameValue() {
		try {
			Controller.updatePriceList(null, "TestPriceList");
		}
		catch (NullPointerException e) {
			assertEquals("Navn er Null", e.getMessage());
		}
	}

	@Test
	public void TC20UpdatePriceListValueAndNameNull() {
		try {
			Controller.updatePriceList(priceList1, null);
		}
		catch (NullPointerException e) {
			assertEquals("Navn er Null", e.getMessage());
		}
	}

	@Test
	public void TC21UpdatePriceListEmpty() {
		try {
			Controller.updatePriceList(priceList1, "");
		}
		catch (IllegalArgumentException e) {
			assertEquals("Feltet må ikke være tomt", e.getMessage());
		}
	}

	@Test
	public void TC22UpdatePriceListDuplicates() {
		priceList2 = Controller.createPriceList("TestPriceList1");
		try {
			Controller.updatePriceList(priceList2, "TestPriceList");
		}
		catch (IllegalArgumentException e) {
			assertEquals("Prislisten: " + priceList1 + ", eksisterer allerede", e.getMessage());
		}
	}

	@Test
	public void TC23DeletePriceList() {
		assertEquals(Storage.getPriceLists().size(), 1);
		assertEquals(Storage.getPriceLists().get(0).getName(), "TestPriceList");
		Controller.deletePriceList(priceList1);
		assertEquals(Storage.getPriceLists().size(), 0);
	}

	@Test
	public void TC24DeletePriceListNull() {
		try {
			Controller.deletePriceList(null);
		}
		catch (NullPointerException e) {
			assertEquals("Prisliste er Null", e.getMessage());
		}
	}

	// ---------------------------------------------------------------------------------------------------------------------------------
	@Test
	public void T25CreateProduct() {
		assertEquals(product1.getName(), "TestProduct");
	}

	@Test
	public void TC26CreateProductNullCategoryNull() {
		try {
			product1 = Controller.createProduct(null, null);
		}
		catch (NullPointerException e) {
			assertEquals("Navn eller Kategori er Null", e.getMessage());
		}
	}

	@Test
	public void TC27CreateProductNullCategoryValue() {
		try {
			product1 = Controller.createProduct(null, productCategory1);
		}
		catch (NullPointerException e) {
			assertEquals("Navn eller Kategori er Null", e.getMessage());
		}
	}

	@Test
	public void TC28CreateProductValueCategoryNull() {
		try {
			product1 = Controller.createProduct("TestProduct", null);
		}
		catch (NullPointerException e) {
			assertEquals("Navn eller Kategori er Null", e.getMessage());
		}
	}

	@Test
	public void T29CreateProductEmpty() {
		try {
			product1 = Controller.createProduct("", productCategory1);
		}
		catch (IllegalArgumentException e) {
			assertEquals("Feltet må ikke være tomt", e.getMessage());
		}
	}

	@Test
	public void TC30CreateProductDuplicates() {
		try {
			product2 = Controller.createProduct("TestProduct", productCategory1);
		}
		catch (IllegalArgumentException e) {
			assertEquals("Ingen dubletter tilladt", e.getMessage());
		}
	}

	@Test
	public void TC31UpdateProduct() {
		Controller.updateProduct(product1, "TestProduct1");
		assertEquals(Storage.getProducts().get(0).getName(), "TestProduct1");
	}

	@Test
	public void TC32UpdateProductNullAndNameNull() {
		try {
			Controller.updateProduct(null, null);
		}
		catch (NullPointerException e) {
			assertEquals("Produkt er Null", e.getMessage());
		}
	}

	@Test
	public void TC33UpdateProductNullAndNameValue() {
		try {
			Controller.updateProduct(null, "TestProduct");
		}
		catch (NullPointerException e) {
			assertEquals("Produkt er Null", e.getMessage());
		}
	}

	@Test
	public void TC34UpdateProductValueAndNameNull() {
		try {
			Controller.updateProduct(product1, null);
		}
		catch (NullPointerException e) {
			assertEquals("Produkt er Null", e.getMessage());
		}
	}

	@Test
	public void TC35UpdateProductEmpty() {
		try {
			Controller.updateProduct(product1, "");
		}
		catch (IllegalArgumentException e) {
			assertEquals("Feltet må ikke være tomt", e.getMessage());
		}
	}

	@Test
	public void TC36UpdateProductDuplicates() {
		product2 = Controller.createProduct("TestProduct1", productCategory1);
		try {
			Controller.updateProduct(product2, "TestProduct");
		}
		catch (IllegalArgumentException e) {
			assertEquals("Produktet: " + product1 + ", eksisterer allerede", e.getMessage());
		}
	}

	@Test
	public void TC37DeleteProduct() {
		assertEquals(Storage.getProducts().size(), 1);
		assertEquals(Storage.getProducts().get(0).getName(), "TestProduct");
		Controller.deleteProduct(product1);
		assertEquals(Storage.getProducts().size(), 0);
	}

	@Test
	public void TC38DeleteProductNull() {
		try {
			Controller.deleteProduct(null);
		}
		catch (NullPointerException e) {
			assertEquals("Produkt er Null", e.getMessage());
		}
	}

	// ---------------------------------------------------------------------------------------------------------------------------------
	@Test
	public void TC39CreatePrice() {
		assertEquals(price1.getPrice(), 30, 0.001);
	}

	@Test
	public void TC40CreatePricePriceListNullProductNull() {
		try {
			Controller.createPrice(price1.getPrice(), null, null);
		}
		catch (NullPointerException e) {
			assertEquals("Prisliste eller Produkt er Null", e.getMessage());
		}
	}

	@Test
	public void TC41CreatePricePriceListNullProductValue() {
		try {
			Controller.createPrice(price1.getPrice(), null, product1);
		}
		catch (NullPointerException e) {
			assertEquals("Prisliste eller Produkt er Null", e.getMessage());
		}
	}

	@Test
	public void TC42CreatePricePriceListValueProductNull() {
		try {
			Controller.createPrice(price1.getPrice(), priceList1, null);
		}
		catch (NullPointerException e) {
			assertEquals("Prisliste eller Produkt er Null", e.getMessage());
		}
	}

	@Test
	public void TC43CreatePriceNegative() {
		try {
			Controller.createPrice(-30, priceList1, product1);
		}
		catch (IllegalArgumentException e) {
			assertEquals("Prisen må ikke være negativ", e.getMessage());
		}
	}

	@Test
	public void TC44RemovePrice() {
		assertEquals(priceList1.getPriceListPrices().size(), 1);
		assertEquals(priceList1.getPriceListPrices().get(0).getPrice(), 30, 0.001);
		Controller.removePrice(priceList1, product1);
		assertEquals(priceList1.getPriceListPrices().size(), 0);
	}

	@Test
	public void TC45RemovePricePriceListNullProductNull() {
		try {
			Controller.removePrice(null, null);
		}
		catch (NullPointerException e) {
			assertEquals("Prisliste eller Produkt er Null", e.getMessage());
		}
	}

	@Test
	public void TC46RemovePricePriceListNullProductValue() {
		try {
			Controller.removePrice(null, product1);
		}
		catch (NullPointerException e) {
			assertEquals("Prisliste eller Produkt er Null", e.getMessage());
		}
	}

	@Test
	public void TC47RemovePricePriceListValueProductNull() {
		try {
			Controller.removePrice(priceList1, null);
		}
		catch (NullPointerException e) {
			assertEquals("Prisliste eller Produkt er Null", e.getMessage());
		}
	}

	// ---------------------------------------------------------------------------------------------------------------------------------
	@Test
	public void TC48CreateCheckout() {
		assertEquals(price1.getPrice(), 30, 0.001);
		assertEquals(checkout1.getAmount(), 5);
	}

	@Test
	public void TC49CreateCheckoutProductNull() {
		try {
			Controller.createCheckout(price1.getPrice(), 5, null);
		}
		catch (NullPointerException e) {
			assertEquals("Produkt er Null", e.getMessage());
		}
	}

	@Test
	public void TC50CreateCheckoutPriceNegative() {
		try {
			Controller.createCheckout(-1, 5, product1);
		}
		catch (IllegalArgumentException e) {
			assertEquals("Antal må ikke være 0 eller negativ og Pris må ikke være negativ", e.getMessage());
		}
	}

	@Test
	public void TC51CreateCheckoutAmountNegative() {
		try {
			Controller.createCheckout(price1.getPrice(), -1, product1);
		}
		catch (IllegalArgumentException e) {
			assertEquals("Antal må ikke være 0 eller negativ og Pris må ikke være negativ", e.getMessage());
		}
	}

	@Test
	public void TC52CreateCheckoutAmountZero() {
		try {
			Controller.createCheckout(price1.getPrice(), 0, product1);
		}
		catch (IllegalArgumentException e) {
			assertEquals("Antal må ikke være 0 eller negativ og Pris må ikke være negativ", e.getMessage());
		}
	}

	@Test
	public void TC53DeleteCheckout() {
		assertEquals(Storage.getCheckouts().size(), 3);
		assertEquals(Storage.getCheckouts().get(0).getPrice(), 30, 0.001);
		assertEquals(Storage.getCheckouts().get(0).getAmount(), 5);
		assertEquals(Storage.getCheckouts().get(0).getProduct().getName(), "TestProduct");
		Controller.deleteCheckout(checkout1);
		assertEquals(Storage.getCheckouts().size(), 2);
	}

	@Test
	public void TC54DeleteCheckoutProductNull() {
		try {
			Controller.deleteCheckout(null);
		}
		catch (NullPointerException e) {
			assertEquals("Checkout er Null", e.getMessage());
		}
	}

	@Test
	public void TC55CreateSale() {
		assertEquals(sale1.getPaymentMethod(), "Kontant");
	}

	@Test
	public void TC56CreateSaleNull() {
		try {
			Controller.createSale(null);
		}
		catch (NullPointerException e) {
			assertEquals("Betalingsmetode er Null", e.getMessage());
		}
	}

	@Test
	public void TC57CreateSaleEmpty() {
		try {
			sale1 = Controller.createSale("");
		}
		catch (IllegalArgumentException e) {
			assertEquals("Feltet må ikke være tomt", e.getMessage());
		}
	}

	// ---------------------------------------------------------------------------------------------------------------------------------
	@Test
	public void TC58CalculatePriceForCheckouts() {
		List<Checkout> checkouts = new ArrayList<>();
		checkouts.add(checkout1);
		checkouts.add(checkout2);
		assertEquals(Controller.calculatePriceForCheckouts(checkouts), 240, 0.001);
	}

	// ---------------------------------------------------------------------------------------------------------------------------------
	@Test
	public void TC59createCheckoutCashDiscount() {
		checkout1 = Controller.createCheckoutCashDiscount(price1.getPrice(), 10, 2, product1);
		assertEquals(checkout1.getProductPriceSum(), 50.0, 0.1);
	}

	@Test
	public void TC60createCheckoutCashDiscount() {
		try {
			checkout1 = Controller.createCheckoutCashDiscount(price1.getPrice(), 70, 2, product1);
		}
		catch (IllegalArgumentException e) {
			assertEquals("Antal må ikke være 0 eller negativ og Pris må ikke være negativ", e.getMessage());
		}
	}

	@Test
	public void TC61createCheckoutCashDiscount() {
		try {
			checkout1 = Controller.createCheckoutCashDiscount(price1.getPrice(), 10, 0, product1);
		}
		catch (IllegalArgumentException e) {
			assertEquals("Antal må ikke være 0 eller negativ og Pris må ikke være negativ", e.getMessage());
		}
	}

	// ---------------------------------------------------------------------------------------------------------------------------------
	@Test
	public void TC62createCheckoutPercentageDiscount() {
		checkout1 = Controller.createCheckoutPercentageDiscount(price1.getPrice(), 10, 2, product1);
		assertEquals(checkout1.getProductPriceSum(), 54.0, 0.1);
	}

	@Test
	public void TC63createCheckoutPercentageDiscount() {
		try {
			checkout1 = Controller.createCheckoutPercentageDiscount(price1.getPrice(), 101, 2, product1);
		}
		catch (IllegalArgumentException e) {
			assertEquals("Antal må ikke være 0 eller negativ og Pris må ikke være negativ", e.getMessage());
		}
	}

	@Test
	public void TC64createCheckoutPercentageDiscount() {
		try {
			checkout1 = Controller.createCheckoutPercentageDiscount(price1.getPrice(), 10, 0, product1);
		}
		catch (IllegalArgumentException e) {
			assertEquals("Antal må ikke være 0 eller negativ og Pris må ikke være negativ", e.getMessage());
		}
	}

	// ---------------------------------------------------------------------------------------------------------------------------------
	@Test
	public void TC65cashDiscountWithinBoundaries() {
		Controller.cashDiscountWithinBoundaries(50.00, 50.00, 2);
		assertTrue(true);
	}

	@Test
	public void TC66cashDiscountWithinBoundaries() {
		Controller.cashDiscountWithinBoundaries(101.00, 50.00, 2);
		assertFalse(false);
	}

	// ---------------------------------------------------------------------------------------------------------------------------------
	@Test
	public void TC67percentDiscountWithinBoundaries() {
		Controller.percentDiscountWithinBoundaries(50.00);
		assertTrue(true);
	}

	@Test
	public void TC68percentDiscountWithinBoundaries() {
		Controller.percentDiscountWithinBoundaries(101.00);
		assertFalse(false);
	}
}