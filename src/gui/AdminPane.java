package gui;

import java.util.ArrayList;

import controller.Controller;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import model.PriceList;
import model.ProductCategory;

public class AdminPane extends GridPane {
	private ComboBox<?> comboBoxAdminPane;
	private TableView<PriceList> tableViewPriceLists;
	private TextField textFieldCreate;
	private Button buttonCreate;
	private TextField textFieldUpdate;
	private Button buttonUpdate;
	private Button buttonDelete;
	private TableView<ProductCategory> tableViewProductCategories;
	private ArrayList<String> comboBoxList = new ArrayList<>();

	public AdminPane() {
		this.setPadding(new Insets(10, 10, 10, 10));
		this.setHgap(10);
		this.setVgap(10);
		this.setGridLinesVisible(false);
		// --------------------------------------------- Contents here:
		comboBoxList.add("Prislister");
		comboBoxList.add("Produkt kategorier");
		comboBoxAdminPane = createAdminPaneComboBox();
		comboBoxAdminPane.setPromptText("Vælg liste");
		this.add(comboBoxAdminPane, 0, 0, 1, 1);
		tableViewPriceLists = createPriceListTableView();
		tableViewPriceLists.setVisible(false);
		this.add(tableViewPriceLists, 0, 1, 1, 5);
		tableViewProductCategories = createProductCategoryTableView();
		this.add(tableViewProductCategories, 0, 1, 1, 5);
		tableViewProductCategories.setVisible(false);
		textFieldCreate = new TextField();
		this.add(textFieldCreate, 1, 1, 1, 1);
		textFieldCreate.setPromptText("Opret ny");
		textFieldUpdate = new TextField();
		this.add(textFieldUpdate, 1, 2, 1, 1);
		textFieldUpdate.setPromptText("Omdøb");
		buttonCreate = new Button("Opret");
		buttonCreate.setOnAction(event -> this.createButtonAction());
		this.add(buttonCreate, 3, 1, 1, 1);
		buttonUpdate = new Button("Omdøb");
		buttonUpdate.setOnAction(event -> this.updateButtonAction());
		this.add(buttonUpdate, 3, 2, 1, 1);
		buttonDelete = new Button("Slet");
		buttonDelete.setOnAction(event -> this.deleteButtonAction());
		GridPane.setValignment(buttonDelete, VPos.BOTTOM);
		this.add(buttonDelete, 1, 5, 1, 1);
		ChangeListener<String> adminChangeListener = (ov, oldAdmin, newAdmin) -> updateListProductsForAdmin(newAdmin);
		// Admin Product ends here.
		// -------------------------------------- Content ends here:
	}

	private ComboBox<String> createAdminPaneComboBox() {
		ComboBox<String> comboBoxAdminPane = new ComboBox<>(FXCollections.observableArrayList(comboBoxList));
		ChangeListener<String> adminChangeListener = (ov, oldAdmin, newAdmin) -> updateListProductsForAdmin(newAdmin);
		comboBoxAdminPane.getSelectionModel().selectedItemProperty().addListener(adminChangeListener);
		return comboBoxAdminPane;
	}

	private void updateListProductsForAdmin(String newAdmin) {
		if (newAdmin.equalsIgnoreCase("Prislister")) {
			showAndHidePriceListElements();
			ArrayList<PriceList> priceLists = Controller.getPriceLists();
			ObservableList<PriceList> pricelistwrapper = FXCollections.observableArrayList(priceLists);
			tableViewPriceLists.getItems().setAll(pricelistwrapper);
		}
		else if (newAdmin.equalsIgnoreCase("Produkt kategorier")) {
			showAndHideProductCategoryElements();
			ArrayList<ProductCategory> productCategories = Controller.getProductCategories();
			ObservableList<ProductCategory> categorywrapper = FXCollections.observableArrayList(productCategories);
			tableViewProductCategories.getItems().setAll(categorywrapper);
		}
	}

	private TableView<PriceList> createPriceListTableView() {
		// populate productname column with products.
		TableColumn<PriceList, String> priceListColumn = new TableColumn<>("Prisliste:");
		priceListColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
		priceListColumn.setStyle("-fx-alignment: CENTER;");
		// create tableview and setup style.
		TableView<PriceList> tableViewPriceLists = new TableView<>();
		tableViewPriceLists.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		tableViewPriceLists.getColumns().addAll(priceListColumn);
		return tableViewPriceLists;
	}

	private TableView<ProductCategory> createProductCategoryTableView() {
		// populate productname column with products.
		TableColumn<ProductCategory, String> productCategoryColumn = new TableColumn<>("Produktkategorier:");
		productCategoryColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCategory()));
		productCategoryColumn.setStyle("-fx-alignment: CENTER;");
		// create tableview and setup style.
		TableView<ProductCategory> tableViewProductCategories = new TableView<>();
		tableViewProductCategories.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		tableViewProductCategories.getColumns().addAll(productCategoryColumn);
		return tableViewProductCategories;
	}
	// -----------------------------------------------
	// enable and diable visibility methods:

	private void showAndHidePriceListElements() {
		tableViewPriceLists.setVisible(true);
		tableViewProductCategories.setVisible(false);
		buttonUpdate.setVisible(true);
		buttonDelete.setVisible(true);
		buttonCreate.setVisible(true);
		textFieldCreate.setVisible(true);
		textFieldUpdate.setVisible(true);
	}

	private void showAndHideProductCategoryElements() {
		tableViewProductCategories.setVisible(true);
		tableViewPriceLists.setVisible(false);
		buttonUpdate.setVisible(true);
		buttonDelete.setVisible(true);
	}

	private void createButtonAction() {
		if (comboBoxAdminPane.getSelectionModel().getSelectedItem().toString().equalsIgnoreCase("Prislister")) {
			String name = textFieldCreate.getText().trim();
			if (name.length() == 0) {
				System.out.println("Prisliste skal have et navn");
			}
			else {
				Controller.createPriceList(name);
				ArrayList<PriceList> priceListPrices = Controller.getPriceLists();
				ObservableList<PriceList> wrapper = FXCollections.observableArrayList(priceListPrices);
				tableViewPriceLists.getItems().setAll(wrapper);
				textFieldCreate.clear();
			}
		}
		if (comboBoxAdminPane.getSelectionModel().getSelectedItem().toString().equalsIgnoreCase("Produkt kategorier")) {
			String name = textFieldCreate.getText().trim();
			if (name.isEmpty()) {
				System.out.println("Produkt kategori skal have et navn");
			}
			else {
				Controller.createProductCategory(name);
				ArrayList<ProductCategory> productcategories = Controller.getProductCategories();
				ObservableList<ProductCategory> wrapper = FXCollections.observableArrayList(productcategories);
				tableViewProductCategories.getItems().setAll(wrapper);
				textFieldCreate.clear();
			}
		}
	}

	private void updateButtonAction() {
		if (comboBoxAdminPane.getSelectionModel().getSelectedItem().toString().equalsIgnoreCase("Prislister")) {
			String name = textFieldUpdate.getText().trim();
			PriceList oldValue = tableViewPriceLists.getSelectionModel().getSelectedItem();
			if (name.length() == 0) {
				System.out.println("Prisliste skal have et navn");
			}
			if (oldValue == null) {
				System.out.println("Vælg den prisliste der skal ændres");
			}
			else {
				Controller.updatePriceList(oldValue, name);
				ArrayList<PriceList> priceListPrices = Controller.getPriceLists();
				ObservableList<PriceList> wrapper = FXCollections.observableArrayList(priceListPrices);
				tableViewPriceLists.getItems().setAll(wrapper);
				textFieldUpdate.clear();
			}
		}
		if (comboBoxAdminPane.getSelectionModel().getSelectedItem().toString().equalsIgnoreCase("Produkt kategorier")) {
			String name = textFieldUpdate.getText().trim();
			ProductCategory oldValue = tableViewProductCategories.getSelectionModel().getSelectedItem();
			if (name.length() == 0) {
				System.out.println("Produktkategori skal have et navn");
			}
			if (oldValue == null) {
				System.out.println("Vælg den kategori der skal ændres");
			}
			else {
				Controller.updateProductCategory(oldValue, name);
				ArrayList<ProductCategory> productcategories = Controller.getProductCategories();
				ObservableList<ProductCategory> wrapper = FXCollections.observableArrayList(productcategories);
				tableViewProductCategories.getItems().setAll(wrapper);
				textFieldUpdate.clear();
			}
		}
	}

	private void deleteButtonAction() {
		if (comboBoxAdminPane.getSelectionModel().getSelectedItem().toString().equalsIgnoreCase("Prislister")) {
			PriceList oldPLValue = tableViewPriceLists.getSelectionModel().getSelectedItem();
			if (oldPLValue == null) {
				System.out.println("Vælg den prisliste der skal slettes");
			}
			else {
				Controller.deletePriceList(oldPLValue);
				ArrayList<PriceList> priceListPrices = Controller.getPriceLists();
				ObservableList<PriceList> wrapper = FXCollections.observableArrayList(priceListPrices);
				tableViewPriceLists.getItems().setAll(wrapper);
			}
		}
		if (comboBoxAdminPane.getSelectionModel().getSelectedItem().toString().equalsIgnoreCase("Produkt kategorier")) {
			ProductCategory oldPCValue = tableViewProductCategories.getSelectionModel().getSelectedItem();
			if (oldPCValue == null) {
				System.out.println("Vælg den produkt kategori der skal slettes");
			}
			else {
				Controller.deleteProductCategory(oldPCValue);
				ArrayList<ProductCategory> productcategories = Controller.getProductCategories();
				ObservableList<ProductCategory> wrapper = FXCollections.observableArrayList(productcategories);
				tableViewProductCategories.getItems().setAll(wrapper);
			}
		}
	}
}
