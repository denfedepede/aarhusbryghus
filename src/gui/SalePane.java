package gui;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import controller.Controller;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import model.*;
import storage.Storage;

public class SalePane extends GridPane {
	// PriceLists starts here:
	private ComboBox<PriceList> comboBoxPricelists;
	private TableView<Price> tableViewPriceListProducts;
	private Spinner<Integer> spinnerAmountOfProducts;
	private Button buttonAddProduct;
	private TableView<Checkout> tableViewCheckout;
	private TextField textFieldTotalPrice;
	private Button buttonBuy;
	private Button buttonRemoveProduct;
	private Button buttonDeleteAllProductsInList;
	private TextField textFieldAddDiscount;
	private Button buttonAddDiscountCash;
	private Button buttonAddDiscountPercent;
	// PriceLists ends here.

	public SalePane() {
		this.setPadding(new Insets(20));
		this.setHgap(20);
		this.setVgap(10);
		this.setGridLinesVisible(false);
		// Contents starts here:
		comboBoxPricelists = createPricelistComboBox();
		comboBoxPricelists.setPromptText("Vælg prisliste");
		ChangeListener<PriceList> priceListChangeListener = (ov, oldPriceList, newPriceList) -> updateListProductsForPriceList(newPriceList);
		comboBoxPricelists.getSelectionModel().selectedItemProperty().addListener(priceListChangeListener);
		this.add(comboBoxPricelists, 0, 0);
		spinnerAmountOfProducts = new Spinner<Integer>(1, 9999, 0, 1);
		this.add(spinnerAmountOfProducts, 1, 1, 2, 1);
		buttonAddProduct = new Button("Tilføj");
		buttonAddProduct.setOnAction(e -> addProductToCheckout());
		buttonAddProduct.setDisable(true);
		this.add(buttonAddProduct, 3, 1);
		buttonAddDiscountCash = new Button("Tilføj med Kontant Rabat");
		buttonAddDiscountCash.setOnAction(e -> addCashDiscountToProduct());
		this.add(buttonAddDiscountCash, 1, 2);
		textFieldAddDiscount = new TextField();
		textFieldAddDiscount.setMaxWidth(80);
		textFieldAddDiscount.setPromptText("Skriv rabat");
		textFieldAddDiscount.setEditable(true);
		this.add(textFieldAddDiscount, 2, 2);
		buttonAddDiscountPercent = new Button("Tilføj med Procent Rabat");
		buttonAddDiscountPercent.setOnAction(e -> addPercentDiscountToProduct());
		this.add(buttonAddDiscountPercent, 3, 2);
		tableViewPriceListProducts = createProductTableView();
		tableViewPriceListProducts.setOnMouseClicked(e -> enabledbutton());
		this.add(tableViewPriceListProducts, 0, 1, 1, 3);
		tableViewCheckout = createCheckoutTableView();
		this.add(tableViewCheckout, 5, 1, 3, 3);
		Label label = new Label("Samlet pris:");
		this.add(label, 5, 4);
		textFieldTotalPrice = new TextField();
		textFieldTotalPrice.setMaxWidth(80);
		textFieldTotalPrice.setEditable(false);
		this.add(textFieldTotalPrice, 6, 4);
		buttonBuy = new Button("Køb");
		buttonBuy.setOnAction(e -> completeSale());
		buttonBuy.setDisable(true);
		this.add(buttonBuy, 5, 5);
		buttonRemoveProduct = new Button("Fjern");
		buttonRemoveProduct.setOnAction(e -> deleteCheckout());
		this.add(buttonRemoveProduct, 6, 5);
		buttonDeleteAllProductsInList = new Button("Slet alt");
		buttonDeleteAllProductsInList.setOnAction(e -> deleteAllFromCheckout());
		this.add(buttonDeleteAllProductsInList, 7, 5);
		// Content ends here:
	}

	private void enabledbutton() {
		buttonAddProduct.setDisable(false);
	}

	public void refreshPane() {
		comboBoxPricelists.getSelectionModel().clearSelection();
		comboBoxPricelists.getItems().setAll(Controller.getPriceLists());
	}

	private ComboBox<PriceList> createPricelistComboBox() {
		ComboBox<PriceList> getcomboBoxPricelists = new ComboBox(FXCollections.observableArrayList(Controller.getPriceLists()));
		return getcomboBoxPricelists;
	}

	private TableView<Price> createProductTableView() {
		// populate productname column with products.
		TableColumn<Price, String> productNameColumn = new TableColumn<>("Produkt:");
		productNameColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getProduct().getName()));
		productNameColumn.setStyle("-fx-alignment: CENTER;");
		// populate productcategory column with categorynames.
		TableColumn<Price, String> productCategoryColumn = new TableColumn<>("Kategori:");
		productCategoryColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getProduct().getProductCategory().getCategory()));
		productCategoryColumn.setStyle("-fx-alignment: CENTER;");
		// populate price column with product prices.
		TableColumn<Price, String> priceColumn = new TableColumn<>("Pris:");
		priceColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getPrice() + ""));
		priceColumn.setStyle("-fx-alignment: CENTER;");
		// create tableview and setup style.
		TableView tableViewPriceListProducts = new TableView();
		tableViewPriceListProducts.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		tableViewPriceListProducts.getColumns().addAll(productNameColumn, productCategoryColumn, priceColumn);
		return tableViewPriceListProducts;
	}

	private TableView<Checkout> createCheckoutTableView() {
		DecimalFormat f = new DecimalFormat("##.00");
		TableColumn<Checkout, String> productName = new TableColumn<>("Produkt:");
		productName.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getProduct().getName()));
		TableColumn<Checkout, String> amount = new TableColumn<>("Antal:");
		amount.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getAmount() + ""));
		TableColumn<Checkout, String> totalPrice = new TableColumn<>("Pris:");
		totalPrice.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getProductPriceSum() + ""));
		TableColumn<Checkout, String> discount = new TableColumn<>("Rabat:");
		discount.setCellValueFactory(cellData -> new SimpleStringProperty("-" + f.format(cellData.getValue().getDiscount())));
		ObservableList<Checkout> checkouts = FXCollections.observableArrayList(Controller.getCheckouts());
		TableView<Checkout> tableViewCheckout = new TableView(checkouts);
		// listview styles
		tableViewCheckout.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		tableViewCheckout.getColumns().addAll(productName, amount, totalPrice, discount);
		return tableViewCheckout;
	}
	// -----------------------------------------------

	private void updateListProductsForPriceList(PriceList newPriceList) {
		ObservableList<Price> prices = tableViewPriceListProducts.getItems();
		// Doing a nullcheck, as pane change causes the selection to reset, which causes
		// the changeListener to be called and producing a NullPointerException if not
		// handled.
		if (newPriceList == null) {
			prices.clear();
			comboBoxPricelists.setPromptText("Vælg prisliste");
			return;
		}
		ArrayList<Price> priceListPrices = newPriceList.getPriceListPrices();
		ObservableList<Price> wrapper = FXCollections.observableArrayList(priceListPrices);
		prices.setAll(wrapper);
	}

	private void addCashDiscountToProduct() {
		Double discounts = getDiscount();
		Price p = tableViewPriceListProducts.getSelectionModel().getSelectedItem();
		Integer amount = spinnerAmountOfProducts.getValue();
		ObservableList<Checkout> checkouts = tableViewCheckout.getItems();
		if (p != null && Controller.cashDiscountWithinBoundaries(discounts, p.getPrice(), amount)) {
			Checkout checkout = Controller.createCheckoutCashDiscount(p.getPrice(), discounts, amount, p.getProduct());
			checkouts.add(checkout);
			textFieldAddDiscount.clear();
		}
		textFieldTotalPrice.setText(Controller.calculatePriceForCheckouts(checkouts) + "");
		disableBuyButtonIfNoCheckouts(checkouts);
	}

	private Double getDiscount() {
		Double discounts = 0.0;
		try {
			discounts = Double.parseDouble(textFieldAddDiscount.getText().trim());
		}
		catch (NumberFormatException e) {
			textFieldAddDiscount.setText("0");
		}
		return discounts;
	}

	private void addPercentDiscountToProduct() {
		Double discounts = getDiscount();
		Price p = tableViewPriceListProducts.getSelectionModel().getSelectedItem();
		Integer amount = spinnerAmountOfProducts.getValue();
		ObservableList<Checkout> checkouts = tableViewCheckout.getItems();
		if (p != null && Controller.percentDiscountWithinBoundaries(discounts)) {
			Checkout checkout = Controller.createCheckoutPercentageDiscount(p.getPrice(), discounts, amount, p.getProduct());
			checkouts.add(checkout);
			textFieldAddDiscount.clear();
		}
		textFieldTotalPrice.setText(Controller.calculatePriceForCheckouts(checkouts) + "");
		disableBuyButtonIfNoCheckouts(checkouts);
	}

	private void addProductToCheckout() {
		Price p = tableViewPriceListProducts.getSelectionModel().getSelectedItem();
		Integer amount = spinnerAmountOfProducts.getValue();
		ObservableList<Checkout> checkouts = tableViewCheckout.getItems();
		if (p != null) {
			Checkout checkout = Controller.createCheckout(p.getPrice(), amount, p.getProduct());
			checkouts.add(checkout);
		}
		textFieldTotalPrice.setText(Controller.calculatePriceForCheckouts(checkouts) + "");
		disableBuyButtonIfNoCheckouts(checkouts);
	}

	private void deleteAllFromCheckout() {
		ObservableList<Checkout> checkouts = tableViewCheckout.getItems();
		for (Checkout checkout : checkouts) {
			Controller.deleteCheckout(checkout);
		}
		for (int i = 0; i < checkouts.size(); i++)
			checkouts.clear();
		textFieldTotalPrice.clear();
		tableViewCheckout.refresh();
		disableBuyButtonIfNoCheckouts(checkouts);
	}

	private void deleteCheckout() {
		Checkout selectedItem = tableViewCheckout.getSelectionModel().getSelectedItem();
		ObservableList<Checkout> checkouts = tableViewCheckout.getItems();
		if (selectedItem != null) {
			Storage.removeCheckout(selectedItem);
			checkouts.remove(selectedItem);
			textFieldTotalPrice.setText(Controller.calculatePriceForCheckouts(checkouts) + "");
		}
		disableBuyButtonIfNoCheckouts(checkouts);
	}

	private void disableBuyButtonIfNoCheckouts(List<Checkout> checkouts) {
		if (checkouts.isEmpty()) {
			buttonBuy.setDisable(true);
			return;
		}
		buttonBuy.setDisable(false);
	}

	private void completeSale() {
		Optional<String> result = showPaymentMethodDialog();
		if (!result.isPresent()) {
			showCompleteDialog(true);
			return;
		}
		String paymentMethod = result.get();
		Alert alert = new Alert(AlertType.INFORMATION);
		Sale sale = Controller.createSale(paymentMethod);
		sale.addCheckout(Controller.getCheckouts());
		ObservableList<Checkout> checkouts = tableViewCheckout.getItems();
		for (Checkout checkout : checkouts) {
			Controller.deleteCheckout(checkout);
		}
		for (int i = 0; i < checkouts.size(); i++)
			checkouts.clear();
		textFieldTotalPrice.clear();
		tableViewCheckout.refresh();
		buttonBuy.setDisable(true);
		showCompleteDialog(false);
	}

	private Optional<String> showPaymentMethodDialog() {
		List<String> paymentmethods = new ArrayList<>();
		paymentmethods.add("Mobilepay");
		paymentmethods.add("Betalingskort");
		paymentmethods.add("Kontant");
		ChoiceDialog<String> paymentmethodDialog = new ChoiceDialog<>("Kontant", paymentmethods);
		Stage stage = (Stage) paymentmethodDialog.getDialogPane().getScene().getWindow();
		paymentmethodDialog.setHeaderText("");
		Optional<String> result = paymentmethodDialog.showAndWait();
		return result;
	}

	private void showCompleteDialog(boolean cancelled) {
		Alert alert = new Alert(AlertType.INFORMATION);
		if (cancelled) {
			alert.setTitle("Køb anulleret");
			alert.setContentText("Køb ikke udført.");
		}
		else {
			alert.setTitle("Køb Afsluttet");
			alert.setContentText("Betalingsform registreret og køb afsluttet.");
		}
		alert.setHeaderText(null);
		alert.showAndWait();
	}
}
