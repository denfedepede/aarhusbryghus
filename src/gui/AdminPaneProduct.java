package gui;

import java.util.ArrayList;

import controller.Controller;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import model.PriceList;
import model.Product;
import model.ProductCategory;

public class AdminPaneProduct extends GridPane {
	private TextField textFieldCreate;
	private Button buttonCreate;
	private TextField textFieldUpdate;
	private Button buttonUpdate;
	private Button buttonDelete;
	private TableView<Product> tableViewProducts;
	private TextField textFieldProductPrice;
	private ComboBox<ProductCategory> comboBoxProductProductCategory;
	private ComboBox<PriceList> comboBoxProductPricelists;
	private Button buttonProductAddToPriceList;
	private Button buttonProductRemoveFromPriceList;
	private ListView<PriceList> listViewPriceList;

	public AdminPaneProduct() {
		this.setPadding(new Insets(10, 10, 10, 10));
		this.setHgap(10);
		this.setVgap(10);
		this.setGridLinesVisible(false);
		// --------------------------------------------- Contents here:
		tableViewProducts = createProductTableView();
		this.add(tableViewProducts, 0, 1, 1, 6);
		ChangeListener<Product> productView = (ov, oldProduct, newProduct) -> this.selectedProduct();
		tableViewProducts.getSelectionModel().selectedItemProperty().addListener(productView);
		ChangeListener<Product> productPriceListChangeListener = (ov, oldProduct, newProduct) -> fillSelectedProductPriceLists(newProduct);
		tableViewProducts.getSelectionModel().selectedItemProperty().addListener(productPriceListChangeListener);
		textFieldCreate = new TextField();
		this.add(textFieldCreate, 1, 1, 1, 1);
		textFieldCreate.setPromptText("Opret ny");
		textFieldCreate.getLayoutX();
		textFieldUpdate = new TextField();
		this.add(textFieldUpdate, 1, 2, 1, 1);
		textFieldUpdate.setPromptText("Omdøb");
		textFieldProductPrice = new TextField();
		textFieldProductPrice.setPromptText("Indtast pris");
		this.add(textFieldProductPrice, 2, 4, 1, 1);
		buttonCreate = new Button("Opret");
		buttonCreate.setOnAction(event -> this.createButtonAction());
		this.add(buttonCreate, 3, 1, 1, 1);
		buttonUpdate = new Button("Omdøb");
		buttonUpdate.setOnAction(e -> updateButtonAction(textFieldUpdate.getText().trim(), (Product) tableViewProducts.getSelectionModel().getSelectedItem()));
		this.add(buttonUpdate, 3, 2, 1, 1);
		buttonProductAddToPriceList = new Button("Tilføj til prisliste");
		buttonProductAddToPriceList.setOnAction(event -> this.addProductToPriceListButton());
		this.add(buttonProductAddToPriceList, 3, 4, 1, 1);
		buttonProductRemoveFromPriceList = new Button("Fjern fra prisliste");
		buttonProductRemoveFromPriceList.setOnAction(event -> this.removeProductFromPricelist());
		this.add(buttonProductRemoveFromPriceList, 3, 5, 1, 1);
		buttonDelete = new Button("Slet");
		buttonDelete.setOnAction(event -> this.deleteButtonAction());
		GridPane.setValignment(buttonDelete, VPos.BOTTOM);
		this.add(buttonDelete, 1, 6, 1, 1);
		comboBoxProductProductCategory = createProductCategoryComboBox();
		this.add(comboBoxProductProductCategory, 2, 1, 1, 1);
		comboBoxProductPricelists = createPriceListComboBox();
		comboBoxProductPricelists.setPromptText("Vælg Prisliste:");
		this.add(comboBoxProductPricelists, 1, 4, 1, 1);
		listViewPriceList = new ListView<>();
		this.add(listViewPriceList, 0, 7);
		listViewPriceList.setPrefHeight(100);
		listViewPriceList.setMouseTransparent(true);
		listViewPriceList.setFocusTraversable(false);
		// Admin Product ends here.
		// -------------------------------------- Content ends here:
	}

	public ComboBox<PriceList> createPriceListComboBox() {
		ObservableList<PriceList> wrapper = FXCollections.observableArrayList(Controller.getPriceLists());
		ComboBox<PriceList> comboBoxPriceList = new ComboBox<>();
		comboBoxPriceList.setPromptText("Vælg Prisliste:");
		comboBoxPriceList.getItems().setAll(wrapper);
		return comboBoxPriceList;
	}

	public ComboBox<ProductCategory> createProductCategoryComboBox() {
		ObservableList<ProductCategory> wrapper = FXCollections.observableArrayList(Controller.getProductCategories());
		ComboBox<ProductCategory> comboBoxProductCategory = new ComboBox<>();
		comboBoxProductCategory.setPromptText("Vælg Produkt Kategori:");
		comboBoxProductCategory.getItems().setAll(wrapper);
		return comboBoxProductCategory;
	}

	public void refreshPane() {
		ObservableList<Product> wrapper = FXCollections.observableArrayList(Controller.getProducts());
		tableViewProducts.getItems().setAll(wrapper);
		comboBoxProductPricelists.getItems().setAll(Controller.getPriceLists());
		comboBoxProductProductCategory.getItems().setAll(Controller.getProductCategories());
	}

	@SuppressWarnings("unchecked")
	private TableView<Product> createProductTableView() {
		// populate productname column with products.
		TableColumn<Product, String> productNameColumn = new TableColumn<>("Produkt:");
		productNameColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
		productNameColumn.setStyle("-fx-alignment: CENTER;");
		// populate productcategory column with categorynames.
		TableColumn<Product, String> productCategoryColumn = new TableColumn<>("Kategori:");
		productCategoryColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getProductCategory().getCategory()));
		productCategoryColumn.setStyle("-fx-alignment: CENTER;");
		// create tableview and setup style.
		TableView<Product> tableViewProducts = new TableView<>();
		tableViewProducts.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		tableViewProducts.getColumns().addAll(productNameColumn, productCategoryColumn);
		return tableViewProducts;
	}

	private void selectedProduct() {
		Product p = tableViewProducts.getSelectionModel().getSelectedItem();
		if (p != null) {
			textFieldUpdate.setText(p.getName());
		}
	}

	// -----------------------------------------------
	// enable and disable visibility methods:
	private void createButtonAction() {
		String name = textFieldCreate.getText().trim();
		ProductCategory pC = (ProductCategory) comboBoxProductProductCategory.getSelectionModel().getSelectedItem();
		if (name.length() == 0) {
			System.out.println("Produkt skal have et navn");
		}
		else {
			Controller.createProduct(name, pC);
			refreshPane();
			textFieldCreate.clear();
		}
	}

	private void fillSelectedProductPriceLists(Product product) {
		ArrayList<PriceList> priceListWithProduct = Controller.getPriceListWithProduct(product);
		ObservableList<PriceList> priceLists = FXCollections.observableArrayList(priceListWithProduct);
		listViewPriceList.getItems().setAll(priceLists);
	}

	private void updateButtonAction(String name, Product product) {
		Controller.updateProduct(product, name);
		refreshPane();
		textFieldUpdate.clear();
	}

	private void deleteButtonAction() {
		Product p = tableViewProducts.getSelectionModel().getSelectedItem();
		if (p != null) {
			Controller.deleteProduct(p);
			refreshPane();
		}
	}

	private void addProductToPriceListButton() {
		Product product = tableViewProducts.getSelectionModel().getSelectedItem();
		PriceList pricelist = (PriceList) comboBoxProductPricelists.getSelectionModel().getSelectedItem();
		Double price = Double.parseDouble(textFieldProductPrice.getText().trim());
		Controller.createPrice(price, pricelist, product);
		textFieldProductPrice.clear();
		textFieldProductPrice.setPromptText("Indtast pris");
		refreshPane();
	}

	private void removeProductFromPricelist() {
		Product product = tableViewProducts.getSelectionModel().getSelectedItem();
		PriceList pricelist = (PriceList) comboBoxProductPricelists.getSelectionModel().getSelectedItem();
		Controller.removePrice(pricelist, product);
		listViewPriceList.refresh();
		textFieldProductPrice.clear();
		textFieldProductPrice.setPromptText("Indtast pris");
		fillSelectedProductPriceLists(product);
		refreshPane();
	}
}
