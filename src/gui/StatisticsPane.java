package gui;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

import controller.Controller;
import model.Product;
import model.Sale;
import model.Checkout;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.chart.PieChart;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;

public class StatisticsPane extends GridPane {
	// Statistics starts here:
	private TextArea txaOversigt;
	private LocalDate today = LocalDate.now();
	private ArrayList<Sale> todaysSale = new ArrayList<>();
	private PieChart pChart;

	// Statistics ends here.
	public StatisticsPane() {
		this.setPadding(new Insets(20));
		this.setHgap(20);
		this.setVgap(10);
		this.setGridLinesVisible(false);
		DatePicker dp = new DatePicker();
		dp.setValue(LocalDate.now());
		GridPane.setValignment(dp, VPos.TOP);
		this.add(dp, 0, 0);
		pChart = new PieChart(pcData());
		pChart.setTitle("Salg");
		pChart.setLegendVisible(false);
		pChart.setPrefWidth(400);
		this.add(pChart, 2, 0, 1, 3);
		txaOversigt = new TextArea();
		txaOversigt.setEditable(false);
		txaOversigt.setPrefWidth(300);
		this.add(txaOversigt, 0, 1, 1, 3);
		ChangeListener<LocalDate> dpStartListener = (ov, oldValue, newValue) -> datePicked(dp);
		dp.valueProperty().addListener(dpStartListener);
		// Content ends here.
	}

	public void updateControls() {
		txaOversigt.clear();
		DatePicker dp = new DatePicker();
		dp.setValue(LocalDate.now());
		fillTodaysSale();
		addSale();
		pChart.setData(pcData());
	}

	private void datePicked(DatePicker dp) {
		today = dp.getValue();
		txaOversigt.clear();
		fillTodaysSale();
		addSale();
		pChart.setData(pcData());
	}

	// Creates an ObservableList for the PieChart:
	private ObservableList<PieChart.Data> pcData() {
		HashMap<Product, Integer> hashMapProducts = new HashMap<>();
		for (Sale s : todaysSale) {
			for (Checkout checkout : s.getCheckouts()) {
				Product p = checkout.getProduct();
				if (hashMapProducts.containsKey(p)) {
					hashMapProducts.put(p, hashMapProducts.get(p) + checkout.getAmount());
				}
				else {
					hashMapProducts.put(p, checkout.getAmount());
				}
			}
		}
		return FXCollections.observableArrayList(fillObservableList(hashMapProducts));
	}

	private ArrayList<PieChart.Data> fillObservableList(HashMap<Product, Integer> map) {
		ArrayList<PieChart.Data> pieChartData = new ArrayList<>();
		for (Product product : map.keySet()) {
			PieChart.Data piechartData = new PieChart.Data(product.getName(), map.get(product));
			pieChartData.add(piechartData);
		}
		return pieChartData;
	}

	private void fillTodaysSale() {
		todaysSale.clear();
		for (Sale sale : Controller.getSales()) {
			if (sale.getDate().isEqual(today) && sale.getCheckouts().size() > 0 && sale.getPaymentMethod() != null) {
				todaysSale.add(sale);
			}
		}
	}

	private void addSale() {
		int i = 1;
		for (Sale sale : todaysSale) {
			StringBuffer temp = new StringBuffer();
			temp.append(String.format("Salg %d: %n", i));
			for (Checkout checkout : sale.getCheckouts()) {
				Product product = checkout.getProduct();
				temp.append(String.format("%s, %d stk.	%.2f kr. stk. %nRabat: %.2f kr. %n", product.getName(), checkout.getAmount(), checkout.getPrice(), checkout.getDiscount()));
			}
			temp.append(String.format("Total: %.2f %nBetalt med: %s", sale.totalPrice(), sale.getPaymentMethod()));
			if (sale.getPaymentMethod().startsWith("Klippekort")) {
				temp.append(" klip");
			}
			temp.append(String.format("%n---------------%n"));
			i++;
			txaOversigt.appendText(temp.toString());
		}
	}
}
