package storage;

import java.util.ArrayList;
import model.*;

public class Storage {
	private static ArrayList<ProductCategory> productCategories = new ArrayList<>();
	private static ArrayList<PriceList> priceLists = new ArrayList<>();
	private static ArrayList<Sale> sales = new ArrayList<>();
	private static ArrayList<Checkout> checkouts = new ArrayList<>();
	private static ArrayList<Product> products = new ArrayList<>();

	// ---------------------------------------------------------------------------------------------------------------------------------
	public static ArrayList<ProductCategory> getProductCategories() {
		return new ArrayList<ProductCategory>(productCategories);
	}

	public static void addProductCategory(ProductCategory productCategory) {
		productCategories.add(productCategory);
	}

	public static void removeProductCategory(ProductCategory productCategory) {
		productCategories.remove(productCategory);
	}

	// ---------------------------------------------------------------------------------------------------------------------------------
	public static ArrayList<PriceList> getPriceLists() {
		return priceLists;
	}

	public static void addPriceList(PriceList priceList) {
		priceLists.add(priceList);
	}

	public static void removePriceList(PriceList priceList) {
		priceLists.remove(priceList);
	}

	// ---------------------------------------------------------------------------------------------------------------------------------
	public static ArrayList<Sale> getSales() {
		return new ArrayList<Sale>(sales);
	}

	public static void addSales(Sale sale) {
		sales.add(sale);
	}

	public static void removeSales(Sale sale) {
		sales.remove(sale);
	}

	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void addCheckout(Checkout checkout) {
		checkouts.add(checkout);
	}

	public static void removeCheckout(Checkout checkout) {
		checkouts.remove(checkout);
	}

	public static ArrayList<Checkout> getCheckouts() {
		return checkouts;
	}

	// ---------------------------------------------------------------------------------------------------------------------------------
	public static ArrayList<Product> getProducts() {
		return products;
	}

	public static void addProduct(Product p) {
		products.add(p);
	}

	public static void removeProduct(Product p) {
		products.remove(p);
	}
}