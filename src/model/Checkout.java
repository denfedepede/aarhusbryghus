package model;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Checkout {
	private double price;
	private double discount;
	private int amount;
	private Product product;

	public Checkout(double price, int amount, Product product) {
		this.price = price;
		this.amount = amount;
		this.product = product;
	}

	public double getProductPriceSum() {
		return BigDecimal.valueOf(price * amount).setScale(2, RoundingMode.HALF_UP).doubleValue();
	}

	public double getPrice() {
		return price;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public int getAmount() {
		return amount;
	}

	public Product getProduct() {
		return product;
	}
}