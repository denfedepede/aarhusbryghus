package model;
public class Price {
	private double price;
	private Product product;

	public Price(double price, Product product) {
		this.price = price;
		this.product = product;
	}

	public double getPrice() {
		return price;
	}

	public Product getProduct() {
		return product;
	}
}