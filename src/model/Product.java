package model;
public class Product {
	private String name;
	private ProductCategory productCategory;

	public Product(String name, ProductCategory productCategory) {
		this.name = name;
		this.productCategory = productCategory;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ProductCategory getProductCategory() {
		return productCategory;
	}

	@Override
	public String toString() {
		return name;
	}
}