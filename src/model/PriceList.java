package model;

import java.util.ArrayList;

public class PriceList {
	private String name;
	private ArrayList<Price> priceListPrices = new ArrayList<>();

	public PriceList(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Price> getPriceListPrices() {
		return priceListPrices;
	}

	@Override
	public String toString() {
		return name;
	}

	public void addPrice(Price price) {
		priceListPrices.add(price);
	}
}