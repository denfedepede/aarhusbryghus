package model;

import java.time.LocalDate;
import java.util.ArrayList;

public class Sale {
	private String paymentMethod;
	private LocalDate date;
	private ArrayList<Checkout> checkouts = new ArrayList<>();

	public Sale(String paymentMethod) {
		this.paymentMethod = paymentMethod;
		this.date = LocalDate.now();
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public double totalPrice() {
		double total = 0;
		for (int i = 0; i < checkouts.size(); i++) {
			total += checkouts.get(i).getPrice() * checkouts.get(i).getAmount();
		}
		return total;
	}

	public LocalDate getDate() {
		return date;
	}

	public ArrayList<Checkout> getCheckouts() {
		return checkouts;
	}

	public void addCheckout(ArrayList<Checkout> tempCheckouts) {
		for (Checkout checkout : tempCheckouts) {
			checkouts.add(checkout);
		}
	}
}