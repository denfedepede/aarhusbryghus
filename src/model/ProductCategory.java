package model;
public class ProductCategory {
	private String category;

	public ProductCategory(String category) {
		this.category = category;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return category;
	}
}